/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
На лекціЇ Філіп сказав що потрібно щоб "М+" записувало число до пам'яті, "М-" - стирало число з пам'яті, а "MRC" - додавало число при арифметичних діях.
*/

/*
1. Зєжнати верстку з джс 
Потрібно зробити цей калькулятор робочим.
2. Знайти всі кнопки та спрбувати їх вивести у консоль
3. Записти перші числа в память коду 
4. вивести числа на екран
5. додати знаки ар. операцій 
6. Знайти другі числа 
7. Вивести другі числа 
8. Вивести результат операції
*/

const calculate = {
  operand1: "",
  sign: "",
  operand2: "",
  rez: "",
  mem: "",
};

const display = document.querySelector(".display input"),
  btnEq = document.querySelector(".keys .orange"),
  mIcon = document.createElement("div");
mIcon.innerText = "m";
mIcon.classList.add("mIcon");

// https://regexr.com/

document.querySelector(".keys").addEventListener("click", (e) => {
  if (validate(/\d/, e.target.value) || validate(/\./, e.target.value)) {
    if (calculate.sign !== "") {
      if (validate(/\./, e.target.value) && calculate.operand2.includes(".")) {
        e.preventDefault();
      } else if (
        validate(/\./, e.target.value) &&
        !calculate.operand2.includes(".")
      ) {
        // 1------------------------------------------------
        if (calculate.operand2 === "") {
          calculate.operand2 = "0";
        }
        // 2------------------------------------------------------
        calculate.operand2 = `${parseFloat(calculate.operand2)}${
          e.target.value
        }`;
      } else {
        calculate.operand2 += e.target.value;
        // 1-----------------------------------------------------------
        if (/^0\d/.test(calculate.operand2)) {
          calculate.operand2 = calculate.operand2.substring(1);
        }
        // 2-----------------------------------------------------------
        // show(calculate.operand2);
        // btnEq.removeAttribute("disabled");
      }
      show(calculate.operand2);
      btnEq.removeAttribute("disabled");
    } else {
      if (validate(/\./, e.target.value) && calculate.operand1.includes(".")) {
        e.preventDefault();
      } else {
        // 1-------------------------------------------------------------
        if (calculate.operand1 === "") {
          calculate.operand1 = "0";
        }

        // 2-------------------------------------------------------------
        calculate.operand1 += e.target.value;
        // 1----------------------------------------------------------------
        if (/^0\d/.test(calculate.operand1)) {
          calculate.operand1 = calculate.operand1.substring(1);
        }

        // 2----------------------------------------------------------------
        // show(calculate.operand1);
      }
      show(calculate.operand1);
    }
  } else if (validate(/^[+-/*]$/, e.target.value)) {
    if (calculate.operand1 === "" && calculate.rez !== "") {
      calculate.operand1 = calculate.rez;
    } else if (calculate.operand1 !== "" && calculate.operand2 !== "") {
      operation(calculate.sign);
      calculate.operand1 = calculate.rez;
      calculate.operand2 = "";
      show(calculate.rez);
    }
    calculate.sign = e.target.value;
  } else if (validate(/^=$/, e.target.value)) {
    if (calculate.operand2 !== "") {
      operation(calculate.sign);
      show(calculate.rez);
      calculate.operand1 = "";
      calculate.operand2 = "";
      calculate.sign = "";
    } //....
  } else if (validate(/^C$/, e.target.value)) {
    calculate.operand1 = "";
    calculate.operand2 = "";
    calculate.sign = "";
    calculate.rez = "";
    btnEq.setAttribute("disabled", "disabled");
    show("0");
  } else if (validate(/^m\+$/, e.target.value)) {
    calculate.mem = display.value;
    // show("0");
    display.before(mIcon);
  } else if (validate(/^m\-$/, e.target.value)) {
    calculate.mem = "";
    mIcon.remove();
  } else if (validate(/^mrc$/, e.target.value)) {
    if (calculate.mem !== "" && calculate.sign === "") {
      calculate.operand1 = calculate.mem;
      show(calculate.mem);
    }
    if (calculate.mem !== "" && calculate.sign !== "") {
      calculate.operand2 = calculate.mem;
      btnEq.removeAttribute("disabled");
      show(calculate.mem);
    }
  }
});

function show(v) {
  // const d = document.querySelector(".display input");

  // display.value = parseFloat(v);
  display.value = v;
}

const validate = (r, v) => r.test(v);

let operation = (sign) => {
  switch (sign) {
    case "+":
      calculate.rez = `${
        parseFloat(calculate.operand1) + parseFloat(calculate.operand2)
      }`;
      break;
    case "-":
      calculate.rez = `${
        parseFloat(calculate.operand1) - parseFloat(calculate.operand2)
      }`;
      break;
    case "*":
      calculate.rez = `${
        parseFloat(calculate.operand1) * parseFloat(calculate.operand2)
      }`;
      break;
    case "/":
      if (calculate.operand2 === "0") {
        calculate.rez = "ERROR";

        setTimeout(() => {
          calculate.operand1 = "";
          calculate.operand2 = "";
          calculate.sign = "";
          calculate.rez = "";
          btnEq.setAttribute("disabled", "disabled");
          show("0");
        }, 2000);
      } else {
        calculate.rez = `${
          parseFloat(calculate.operand1) / parseFloat(calculate.operand2)
        }`;
      }
      break;
  }
};
